/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.davide.controller;

import com.davide.dao.OfficinaDAOLocal;
import com.davide.model.Officina;
import java.io.IOException;
import javax.ejb.EJB;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author Davide Ditolve 806953
 */
@WebServlet(name = "OfficinaServlet", urlPatterns = {"/OfficinaServlet"})
public class OfficinaServlet extends HttpServlet {

    @EJB
    private OfficinaDAOLocal officinaDao;

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String action = request.getParameter("action");
        request.setAttribute("pagina", "officina");
        int idOfficina = officinaDao.getLastId();
        String indirizzo = request.getParameter("marca");
        int postiAuto = Integer.parseInt(request.getParameter("modello"));
        String nome = request.getParameter("nome");

        Officina officina = new Officina(idOfficina, indirizzo, nome);
        officina.setPostiauto(postiAuto);
        if (action.equalsIgnoreCase("Add")) {
            officinaDao.addOfficina(officina);
        } else if (action.equalsIgnoreCase("Edit")) {
            officinaDao.editOfficina(officina);
        } else if (action.equalsIgnoreCase("delete")) {
            officinaDao.deleteOfficina(idOfficina);
        } else if (action.equalsIgnoreCase("Search")) {
            StaticVariables.getInstance().setLoaded(true);
            request.setAttribute("load", StaticVariables.getInstance().isLoaded());
            officina = officinaDao.getOfficina(idOfficina);
        }
        StaticVariables.getInstance().setLoaded(false);

        request.setAttribute("officina", officina);
        request.setAttribute("allOfficina", officinaDao.getAllOfficine());
        request.getRequestDispatcher("Officina.jsp").forward(request, response);
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
