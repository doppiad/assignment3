<%-- 
    Document   : index
    Created on : 6-dic-2018, 22.43.05
    Author     : Davide
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <!-- Latest compiled and minified CSS -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
<script src="https://code.jquery.com/jquery-3.3.1.min.js"/>
<!-- Optional theme -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css" integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp" crossorigin="anonymous">

<!-- Latest compiled and minified JavaScript -->
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
 <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.5.0/css/all.css" integrity="sha384-B4dIYHKNBt8Bc12p+WXckhzcICo0wtJAoU8YZTY5qE0Id1GSseTk6S+L3BlXeVIU" crossorigin="anonymous">

<title>Informazione meccanici</title>
    </head>
    <body>
        <%@include file="header.jsp" %>
        <h1>Meccanico Information</h1>
        <form action="./MeccanicoServlet" method="POST">
            <table class="table">
                <tr hidden="hidden">
                    <td>ID Cliente</td>
                    <td><input class="form-control" type="hidden" name="id" value="${meccanico.id}" /></td>
                </tr>
                <tr>
                    <td>Nome</td>
                    <td><input class="form-control" type="text" name="nome" value="${meccanico.nome}" /></td>
                </tr>
                <tr>
                    <td>Cognome</td>
                    <td><input class="form-control" type="text" name="cognome" value="${meccanico.cognome}" /></td>
                </tr>
                <tr>
                    <td>Capo Officina</td>
                    <td>
                        <select id="idcapoofficina" name="idcapoofficina">
                            <option value="">Selezionare un capo officina</option>
                        <c:forEach items="${allMeccanico}" var="me">
                            <option value="${me.id}">${me.nome} ${me.cognome}</option>
                        </c:forEach>
                        </select>
                    </td>
                </tr>
                <tr>
                    <td colspan="2">
                        <input class="btn btn-default" type="submit" name="action" value="Add" />
                        <input class="btn btn-default" type="submit" name="action" value="Edit" />

                        <input class="btn btn-default" type="submit" id="load" name="action" value="Search" />
                    </td>                
                </tr>            
            </table>
        </form>        
        <br>
        <table border="1" class="table">
            <th>ID</th>
            <th>Nome</th>
            <th>Cognome</th>
            <th>Nome capofficina</th>
            <th></th>
            <th></th>
            <th></th>
            <c:forEach items="${allMeccanico}" var="me">
                <tr>
                    <td>${me.id}</td>
                    <td>${me.nome}</td>
                    <td>${me.cognome}</td>
                    <td>${me.idcapoofficina.nome}</td>
                    <td>
                        <form action="./MeccanicoServlet" method="POST">
                                    <input type="hidden" name="id" value="${me.id}"/>
                        <input class="btn btn-default" type="submit" name="action" value="Delete" />
                        </form>
                    </td>
                    <td>
                        <form action="./MeccanicoServlet" method="POST">
                                    <input type="hidden" name="id" value="${me.id}"/>
                        <input class="btn btn-default" type="submit" name="action" value="Search" />
                        </form>
                    </td>
                </tr>
            </c:forEach>
        </table>  
        <a href="index.jsp"><i class="fas fa-undo-alt"></i> Back to menu</a>
    </div>
        <script>
        $("#load").trigger("click");
    </script>
    </body>
</html>
