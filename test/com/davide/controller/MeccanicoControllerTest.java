package com.davide.controller;
import java.util.regex.Pattern;
import java.util.concurrent.TimeUnit;
import org.junit.*;
import static org.junit.Assert.*;
import static org.hamcrest.CoreMatchers.*;
import org.openqa.selenium.*;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.support.ui.Select;

public class MeccanicoControllerTest {
  private WebDriver driver;
  private String baseUrl;
  private boolean acceptNextAlert = true;
  private StringBuffer verificationErrors = new StringBuffer();

  @Before
 public void setUp() throws Exception {
        System.setProperty("webdriver.gecko.driver", "c:\\test\\geckodriver.exe");

//Now you can Initialize marionette driver to launch firefox
        DesiredCapabilities capabilities = DesiredCapabilities.firefox();
        capabilities.setCapability("marionette", true);
        driver = new FirefoxDriver();
        baseUrl = "http://localhost:8080/CRUDOfficina/faces/index.xhtml";
        driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
    }

  @Test
  public void testCRUDMeccanico() throws Exception {
    driver.get(baseUrl);
    driver.findElement(By.linkText("Show All Meccanico Items")).click();
    driver.findElement(By.linkText("Create New Meccanico")).click();
    driver.findElement(By.id("j_idt11:idofficina")).click();
    driver.findElement(By.id("j_idt11:idofficina")).click();
    driver.findElement(By.linkText("Index")).click();
    driver.findElement(By.linkText("Show All Officina Items")).click();
    driver.findElement(By.linkText("Create New Officina")).click();
    driver.findElement(By.id("j_idt11:indirizzo")).click();
    driver.findElement(By.id("j_idt11:indirizzo")).clear();
    driver.findElement(By.id("j_idt11:indirizzo")).sendKeys("Via Catania");
    driver.findElement(By.id("j_idt11:postiauto")).clear();
    driver.findElement(By.id("j_idt11:postiauto")).sendKeys("2");
    driver.findElement(By.id("j_idt11:nome")).clear();
    driver.findElement(By.id("j_idt11:nome")).sendKeys("Officina A");
    driver.findElement(By.linkText("Save")).click();
    driver.findElement(By.linkText("Index")).click();
    driver.findElement(By.linkText("Show All Meccanico Items")).click();
    driver.findElement(By.linkText("Create New Meccanico")).click();
    driver.findElement(By.id("j_idt11:nome")).click();
    driver.findElement(By.id("j_idt11:nome")).clear();
    driver.findElement(By.id("j_idt11:nome")).sendKeys("Ubaldo");
    driver.findElement(By.id("j_idt11:cognome")).clear();
    driver.findElement(By.id("j_idt11:cognome")).sendKeys("IlMeccanico");
    new Select(driver.findElement(By.id("j_idt11:idofficina"))).selectByVisibleText("com.davide.model.Officina[ id=1 ]");
    driver.findElement(By.xpath("(.//*[normalize-space(text()) and normalize-space(.)='Idofficina:'])[1]/following::option[2]")).click();
    driver.findElement(By.id("j_idt11")).click();
    driver.findElement(By.linkText("Save")).click();
    driver.findElement(By.linkText("Show All Meccanico Items")).click();
    driver.findElement(By.linkText("View")).click();
    driver.findElement(By.linkText("Edit")).click();
    driver.findElement(By.id("j_idt11:cognome")).click();
    driver.findElement(By.id("j_idt11:cognome")).clear();
    driver.findElement(By.id("j_idt11:cognome")).sendKeys("Acab");
    driver.findElement(By.linkText("Save")).click();
    driver.findElement(By.linkText("Destroy")).click();
    driver.findElement(By.linkText("Index")).click();
    driver.findElement(By.linkText("Show All Officina Items")).click();
    driver.findElement(By.linkText("Index")).click();
  }

  @After
  public void tearDown() throws Exception {
    driver.quit();
    String verificationErrorString = verificationErrors.toString();
    if (!"".equals(verificationErrorString)) {
      fail(verificationErrorString);
    }
  }

  private boolean isElementPresent(By by) {
    try {
      driver.findElement(by);
      return true;
    } catch (NoSuchElementException e) {
      return false;
    }
  }

  private boolean isAlertPresent() {
    try {
      driver.switchTo().alert();
      return true;
    } catch (NoAlertPresentException e) {
      return false;
    }
  }

  private String closeAlertAndGetItsText() {
    try {
      Alert alert = driver.switchTo().alert();
      String alertText = alert.getText();
      if (acceptNextAlert) {
        alert.accept();
      } else {
        alert.dismiss();
      }
      return alertText;
    } finally {
      acceptNextAlert = true;
    }
  }
}
