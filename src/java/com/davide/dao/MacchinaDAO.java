/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.davide.dao;

import com.davide.model.Macchina;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 *
 * @author Davide
 */
@Stateless
public class MacchinaDAO implements MacchinaDAOLocal {

    @PersistenceContext
    private EntityManager em;
    
       @Override
    public void addMacchina(Macchina macchina) {
        em.persist(macchina);
    }

    @Override
    public void editMacchina(Macchina macchina) {
        em.merge(macchina);
    }

    @Override
    public void deleteMacchina(int idMacchina) {
                em.remove(getMacchina(idMacchina));
    }
    @Override
    public Macchina getMacchina(int idMacchina){
        return em.find(Macchina.class, idMacchina);
    }
    
    @Override
    public List<Macchina> getAllMacchine(){
        return em.createNamedQuery("Officina.findAll").getResultList();
    }
    @Override
    public int getLastId(){
        return ((int)em.createNamedQuery("Macchina.lastId").getResultList().get(0))+1;
    }
}
