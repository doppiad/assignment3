/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.davide.dao;

import com.davide.model.Officina;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 *
 * @author Davide
 */
@Stateless
public class OfficinaDAO implements OfficinaDAOLocal {
    @PersistenceContext
    private EntityManager em;
    
    @Override
    public void addOfficina(Officina officina) {
        em.persist(officina);
    }

    @Override
    public void editOfficina(Officina officina) {
        em.merge(officina);
    }

    @Override
    public void deleteOfficina(int idOfficina) {
                        em.remove(getOfficina(idOfficina));
    }
    @Override
    public Officina getOfficina(int idOfficina){
        return em.find(Officina.class, idOfficina);
    }
    @Override
    public List<Officina> getAllOfficine(){
        return em.createNamedQuery("Officina.findAll").getResultList();
    }    
    @Override
    public int getLastId(){
        return ((int)em.createNamedQuery("Officina.lastId").getResultList().get(0))+1;
    }
}
