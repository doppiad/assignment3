/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.davide.controller;

import com.davide.model.Officina;
import java.io.File;
import java.util.HashMap;
import java.util.Map;
import javax.ejb.embeddable.EJBContainer;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Davide Ditolve 806953
 */
public class OfficinaFacadeTest {
    
        EJBContainer container;
    
    public OfficinaFacadeTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
        Map<String, Object> properties = new HashMap<String, Object>();
        properties.put(EJBContainer.MODULES, new File("build\\web\\WEB-INF\\classes"));
        container = javax.ejb.embeddable.EJBContainer.createEJBContainer(properties);
    }
    
    @After
    public void tearDown() {
        container.close();
    }

    /**
     * Test of insert method, of class OfficinaFacade.
     */
    @Test
    public void testInsert() throws Exception
    {
        System.out.println("find");

        OfficinaFacade instance = (OfficinaFacade) container.getContext().lookup("java:global/classes/OfficinaFacade");
        Officina expResult = null;
        Officina o = new Officina();
        o.setIndirizzo("via casa sua");
        o.setNome("nome");
        o.setPostiauto(10);
        o.setId(0);
        instance.create(o);
        Officina result = instance.find(1);
        assertEquals("nome", result.getNome());
        instance.remove(o);
    }

    /**
     * Test of edit method, of class OfficinaFacade.
     */
    @Test
    public void testEdit() throws Exception
    {
        System.out.println("edit");

        OfficinaFacade instance = (OfficinaFacade) container.getContext().lookup("java:global/classes/OfficinaFacade");
        Officina expResult = null;
        Officina o = new Officina();
        o.setIndirizzo("via casa sua");
        o.setNome("nome");
        o.setPostiauto(10);
        o.setId(0);
        instance.create(o);
        Officina result = instance.find(1);
        result.setNome("edit");
        instance.edit(result);
        result = instance.find(instance.count());
        assertEquals("edit", result.getNome());
        instance.remove(o);
    }

    /**
     * Test of find method, of class OfficinaFacade.
     */
     @Test
    public void testFind() throws Exception {
        System.out.println("find");
        Object id = null;

        OfficinaFacade instance = (OfficinaFacade) container.getContext().lookup("java:global/classes/OfficinaFacade");
              Officina o = new Officina();
        o.setIndirizzo("via casa sua");
        o.setNome("nome");
        o.setPostiauto(10);
        o.setId(0);
        instance.create(o);
        Officina expResult = null;
        Officina result = instance.find(1);
        assertEquals("nome", result.getNome());
        // TODO review the generated test code and remove the default call to fail.
    }
    
    /**
     * Test of remove method, of class OfficinaFacade.
     */
    @Test
    public void testDelete() throws Exception {
        System.out.println("delete");
        OfficinaFacade instance = (OfficinaFacade) container.getContext().lookup("java:global/classes/OfficinaFacade");
        Officina expResult = null;
        Officina o = new Officina();
        o.setIndirizzo("via casa sua");
        o.setNome("nome");
        o.setPostiauto(10);
        o.setId(0);
        instance.create(o);
        instance.remove(o);
    }
}
