/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.davide.dao;

import com.davide.model.Meccanico;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 *
 * @author Davide
 */
@Stateless
public class MeccanicoDAO implements MeccanicoDAOLocal {
    @PersistenceContext
    private EntityManager em;
    
    @Override
    public void addMeccanico(Meccanico meccanico) {
        em.persist(meccanico);
    }

    @Override
    public void editMeccanico(Meccanico meccanico) {
        em.merge(meccanico);
    }

    @Override
    public void deleteMeccanico(int idMeccanico) {
        em.remove(getMeccanico(idMeccanico));
    }
    @Override
    public Meccanico getMeccanico(int idMeccanico){
        return em.find(Meccanico.class, idMeccanico);
    }
    @Override
    public List<Meccanico> getAllMeccanici(){
        return em.createNamedQuery("Meccanico.findAll").getResultList();
    }
    @Override
    public int getLastId(){
        return ((int)em.createNamedQuery("Meccanico.lastId").getResultList().get(0))+1;
    }
}
