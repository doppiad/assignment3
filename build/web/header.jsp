<%-- 
    Document   : header
    Created on : 11-dic-2018, 8.54.05
    Author     : Davide
--%>
<style>
    body {
        min-height: 2000px;
        padding-top: 70px;
    }
</style>
<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.5.0/css/all.css" integrity="sha384-B4dIYHKNBt8Bc12p+WXckhzcICo0wtJAoU8YZTY5qE0Id1GSseTk6S+L3BlXeVIU" crossorigin="anonymous">


<!-- Fixed navbar -->
<nav class="navbar navbar-default navbar-fixed-top">
    <div class="container">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="#">Ditolve Davide 806953</a>
        </div>
        <div id="navbar" class="navbar-collapse collapse">
            <ul class="nav navbar-nav">
                <li id="home"><a href="/CRUDOfficina"><i class="fas fa-home"></i> &nbsp;Home</a></li>

                <li id="clienti"><a href="Clienteinfo.jsp"><i class="fas fa-users"></i> &nbsp;Clienti</a></li>
                <li id="macchine"><a href="Macchinainfo.jsp"><i class="fas fa-car"></i> &nbsp;Macchine</a></li>
                <li id="officine"><a href="Officinainfo.jsp"><i class="fas fa-wrench"></i> &nbsp; Officine</a></li>
                <li id="meccanici"><a href="Meccanicoinfo.jsp"><i class="fas fa-user-cog"></i> &nbsp;Meccanici</a></li>
            </ul>
        </div><!--/.nav-collapse -->
    </div>
</nav>
<script>
    if (${pagina==""}||${pagina==null}) {
        $('#home').attr("class", "active");
    }
    if (${pagina=="cliente"}) {
        $('#clienti').attr("class", "active");
    }
        if (${pagina=="macchina"}) {
        $('#macchine').attr("class", "active");
    }
        if (${pagina=="officina"}) {
        $('#officine').attr("class", "active");
    }
        if (${pagina=="meccanico"}) {
        $('#meccanici').attr("class", "active");
    }
</script>
<div class="container">