/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.davide.dao;

import com.davide.model.Meccanico;
import java.util.List;
import javax.ejb.Local;

/**
 *
 * @author Davide
 */
@Local
public interface MeccanicoDAOLocal {
    public void addMeccanico(Meccanico meccanico);

    public void editMeccanico(Meccanico meccanico);
    public void deleteMeccanico(int idMeccanico);
    public Meccanico getMeccanico(int idMeccanico);
    public List<Meccanico> getAllMeccanici();
    public int getLastId();
}
