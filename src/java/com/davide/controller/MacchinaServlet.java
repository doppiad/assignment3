/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.davide.controller;

import com.davide.dao.ClienteDAOLocal;
import com.davide.dao.MacchinaDAOLocal;
import com.davide.model.Macchina;
import java.io.IOException;
import javax.ejb.EJB;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author Davide Ditolve 806953
 */
@WebServlet(name = "MacchinaServlet", urlPatterns = {"/MacchinaServlet"})
public class MacchinaServlet extends HttpServlet {
    @EJB
    private ClienteDAOLocal clienteDao;
    private MacchinaDAOLocal macchinaDao;
    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String action = request.getParameter("action");
        request.setAttribute("pagina", "macchina");
        int idMacchina = macchinaDao.getLastId();
        String marca = request.getParameter("marca");
        String modello = request.getParameter("modello");
        String targa = request.getParameter("targa");
        
        Macchina macchina = new Macchina(idMacchina,marca,modello);
        macchina.setTarga(targa);
        if(action.equalsIgnoreCase("Add")){
            macchinaDao.addMacchina(macchina);
        }
        else if(action.equalsIgnoreCase("Edit"))
        {
            macchinaDao.editMacchina(macchina);
        }
        else if(action.equalsIgnoreCase("delete")){
            macchinaDao.deleteMacchina(idMacchina);
        }        
        else if(action.equalsIgnoreCase("Search")){
                        StaticVariables.getInstance().setLoaded(true);
            request.setAttribute("load", StaticVariables.getInstance().isLoaded());
            macchina = macchinaDao.getMacchina(idMacchina);
        }
        StaticVariables.getInstance().setLoaded(false);
        request.setAttribute("macchina", macchina);
        request.setAttribute("allMacchina",macchinaDao.getAllMacchine());
        request.getRequestDispatcher("Macchinainfo.jsp").forward(request, response);
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
