/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.davide.controller;

import com.davide.dao.MeccanicoDAOLocal;
import com.davide.model.Meccanico;
import java.io.IOException;
import javax.ejb.EJB;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author Davide Ditolve 806953
 */
@WebServlet(name = "MeccanicoServlet", urlPatterns = {"/MeccanicoServlet"})
public class MeccanicoServlet extends HttpServlet {
    @EJB
    private MeccanicoDAOLocal meccanicoDao;
    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String action = request.getParameter("action");
        request.setAttribute("pagina", "meccanico");
        String id = request.getParameter("id");
        int idMeccanico = id!=null&&!id.equals("")?Integer.parseInt(id):meccanicoDao.getLastId();
        String nome = request.getParameter("nome");
        String cognome = request.getParameter("cognome");
        String capoofficina = request.getParameter("idcapoofficina");
        int capoOfficinanumber = capoofficina!=null&&capoofficina!=""?Integer.parseInt(capoofficina):0;
        Meccanico meccanico = new Meccanico(idMeccanico,nome,cognome);
        meccanico.setIdcapoofficina(meccanicoDao.getMeccanico(capoOfficinanumber));
        if(action.equalsIgnoreCase("Add")){
            meccanicoDao.addMeccanico(meccanico);
        }
        else if(action.equalsIgnoreCase("Edit"))
        {
            meccanicoDao.editMeccanico(meccanico);
        }
        else if(action.equalsIgnoreCase("delete")){
            meccanicoDao.deleteMeccanico(idMeccanico);
        }        
        else if(action.equalsIgnoreCase("Search")){
                        StaticVariables.getInstance().setLoaded(true);
            request.setAttribute("load", StaticVariables.getInstance().isLoaded());
            meccanico = meccanicoDao.getMeccanico(idMeccanico);
        }
        StaticVariables.getInstance().setLoaded(false);
        request.setAttribute("meccanico", meccanico);
        request.setAttribute("allMeccanico",meccanicoDao.getAllMeccanici());
        request.getRequestDispatcher("Meccanicoinfo.jsp").forward(request, response);
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
