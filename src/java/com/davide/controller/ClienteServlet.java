/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.davide.controller;

import com.davide.dao.ClienteDAOLocal;
import com.davide.model.Cliente;
import java.io.IOException;
import javax.ejb.EJB;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author Davide Ditolve 806953
 */
@WebServlet(name = "ClienteServlet", urlPatterns = {"/ClienteServlet"})
public class ClienteServlet extends HttpServlet {

    @EJB
    private ClienteDAOLocal clienteDao;

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String action = request.getParameter("action");
        request.setAttribute("pagina", "cliente");
        String idClienteStr = request.getParameter("id");

        int idCliente = 0;
        if (idClienteStr != null && !idClienteStr.equals("")) {
            idCliente = Integer.parseInt(idClienteStr);
        } else {
            idCliente = clienteDao.getLastId();
        }
        String nome = request.getParameter("nome");
        String cognome = request.getParameter("cognome");
        String indirizzo = request.getParameter("indirizzo");
        String telefono = request.getParameter("telefono");

        Cliente cliente = new Cliente(idCliente, nome, cognome, indirizzo);
        cliente.setTelefono(telefono);
        if (action.equalsIgnoreCase("Add")) {
            clienteDao.addCliente(cliente);
        } else if (action.equalsIgnoreCase("Edit")) {
            clienteDao.editCliente(cliente);
        } else if (action.equalsIgnoreCase("delete")) {
            clienteDao.deleteCliente(idCliente);
        } else if (action.equalsIgnoreCase("Search") || action.equalsIgnoreCase("Load")) {
            StaticVariables.getInstance().setLoaded(true);
            request.setAttribute("load", StaticVariables.getInstance().isLoaded());
            cliente = clienteDao.getCliente(idCliente);
        }
        StaticVariables.getInstance().setLoaded(false);
        request.setAttribute("cliente", cliente);
        request.setAttribute("allClienti", clienteDao.getAllClienti());
        request.getRequestDispatcher("Clienteinfo.jsp").forward(request, response);
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
