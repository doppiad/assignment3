/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.davide.dao;

import com.davide.model.Cliente;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 *
 * @author Davide
 */
@Stateless
public class ClienteDAO implements ClienteDAOLocal {
@PersistenceContext(unitName = "CRUDOfficinaPU")    
    private EntityManager em;
    
    @Override
    public void addCliente(Cliente cliente) {
        em.persist(cliente);
    }

    @Override
    public void editCliente(Cliente cliente) {
        em.merge(cliente);
    }

    @Override
    public void deleteCliente(int idCliente) {
        em.remove(getCliente(idCliente));
    }
    @Override
    public Cliente getCliente(int idCliente){
        return em.find(Cliente.class, idCliente);
    }
    @Override
    public List<Cliente> getAllClienti(){
        return em.createNamedQuery("Cliente.findAll").getResultList();
    }
    @Override
    public int getLastId(){
        return ((int)em.createNamedQuery("Cliente.lastId").getResultList().get(0))+1;
    }
}
