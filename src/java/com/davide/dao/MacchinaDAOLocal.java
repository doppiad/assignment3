/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.davide.dao;

import com.davide.model.Macchina;
import java.util.List;
import javax.ejb.Local;

/**
 *
 * @author Davide
 */
@Local
public interface MacchinaDAOLocal {
    public void addMacchina(Macchina macchina);
    public void editMacchina(Macchina macchina);
    public void deleteMacchina(int idMacchina);
    public Macchina getMacchina(int idMacchina);
    public List<Macchina> getAllMacchine();
    public int getLastId();
}
