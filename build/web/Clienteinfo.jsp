<%-- 
    Document   : index
    Created on : 6-dic-2018, 22.43.05
    Author     : Davide
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <!-- Latest compiled and minified CSS -->
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
        <script src="https://code.jquery.com/jquery-3.3.1.min.js"/>
        <!-- Optional theme -->
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css" integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp" crossorigin="anonymous">

        <!-- Latest compiled and minified JavaScript -->
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.5.0/css/all.css" integrity="sha384-B4dIYHKNBt8Bc12p+WXckhzcICo0wtJAoU8YZTY5qE0Id1GSseTk6S+L3BlXeVIU" crossorigin="anonymous">

        <title>Informazione clienti</title>
    </head>
    <body>
        <jsp:include page="header.jsp" />
        <h1>Cliente Information</h1>
        <form action="./ClienteServlet" method="POST" id="form1">
            <table class="table">
                <tr hidden="hidden">
                    <td>ID Cliente</td>
                    <td><input class="form-control" type="hidden" id="resetid" name="id" value="${cliente.id}" /></td>
                </tr>
                <tr>
                    <td>First Name</td>
                    <td><input class="form-control" type="text" id="resetnome" name="nome" value="${cliente.nome}" /></td>
                </tr>
                <tr>
                    <td>Last Name</td>
                    <td><input class="form-control" type="text" id="resetcognome" name="cognome" value="${cliente.cognome}" /></td>
                </tr>
                <tr>
                    <td>Indirizzo</td>
                    <td><input class="form-control" type="text" id="resetindirizzo" name="indirizzo" value="${cliente.indirizzo}" /></td>
                </tr>
                <tr>
                    <td>Telefono</td>
                    <td><input class="form-control" type="text" id="resettelefono" name="telefono" value="${cliente.telefono}" /></td>
                </tr>
                <tr>
                    <td colspan="2">
                        <button class="btn btn-default" type="submit" name="action" value="Add">
                            <i class="fas fa-plus"></i> Aggiungi
                        </button>
                        <c:if test="${cliente.id!=null}">
                            <button class="btn btn-default" type="submit" id="editbtn" name="action" value="Edit">
                                <i class="fas fa-edit"></i> Salva modifiche
                            </button>
                        </c:if>
                        <a href="#" class="btn btn-default"  onclick="resetta();">
                            <i class="fas fa-sync"></i> Ripristina
                        </button>
                        <input class="btn btn-default" style="display:none;" type="submit" name="action" id="load" value="Search" />
                    </td>                
                </tr>            
            </table>
        </form>        
        <br>
        <table border="1" class="table">
            <th>ID</th>
            <th>Nome</th>
            <th>Cognome</th>
            <th>Indirizzo</th>
            <th>Telefono</th>
            <th>Cancella</th>
            <th>Carica</th>
                <c:forEach items="${allClienti}" var="cl">
                <tr>
                    <td>${cl.id}</td>
                    <td>${cl.nome}</td>
                    <td>${cl.cognome}</td>
                    <td>${cl.indirizzo}</td>
                    <td>${cl.telefono}</td>
                    <td>
                        <form action="./ClienteServlet" method="POST">
                            <input type="hidden" name="id" value="${cl.id}">
                            <input class="btn btn-default" type="hidden" name="action" value="Delete">
                            <a href="javascript:;" onclick="parentNode.submit()"><i class="fas fa-trash-alt"></i></a>

                        </form>
                    </td>
                    <td>
                        <form action="./ClienteServlet" method="POST">
                            <input type="hidden" name="id" value="${cl.id}">
                            <input class="btn btn-default" type="hidden" name="action" value="Load">
                            <a href="javascript:;" onclick="parentNode.submit()"><i class="fas fa-file-upload"></i></a>
                        </form>
                    </td>
                </tr>
            </c:forEach>
        </table>  
    </div>
    <script>
        
        function resetta(){
            $('#resettelefono').val('');
            $('#resetid').val('');
            $('#resetnome').val('');
            $('#resetcognome').val('');
            $('#resetindirizzo').val('');
             $('#editbtn').hide();
        }
    </script>
</body>
</html>
