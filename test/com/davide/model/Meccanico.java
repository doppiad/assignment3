/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.davide.model;

import java.io.Serializable;
import java.util.Collection;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Davide Ditolve 806953
 */
@Entity
@Table(name = "MECCANICO")
@NamedQueries({
    @NamedQuery(name = "Meccanico.findAll", query = "SELECT m FROM Meccanico m"),
    @NamedQuery(name = "Meccanico.findById", query = "SELECT m FROM Meccanico m WHERE m.id = :id"),
    @NamedQuery(name = "Meccanico.findByNome", query = "SELECT m FROM Meccanico m WHERE m.nome = :nome"),
    @NamedQuery(name = "Meccanico.findByCognome", query = "SELECT m FROM Meccanico m WHERE m.cognome = :cognome"),
    @NamedQuery(name = "Meccanico.lastId",query ="SELECT max(m.id) FROM Meccanico m")})
public class Meccanico implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "ID")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 255)
    @Column(name = "NOME")
    private String nome;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 255)
    @Column(name = "COGNOME")
    private String cognome;
    @OneToMany(mappedBy = "idcapoofficina")
    private Collection<Meccanico> meccanicoCollection;
    @JoinColumn(name = "IDCAPOOFFICINA", referencedColumnName = "ID")
    @ManyToOne
    private Meccanico idcapoofficina;
    @JoinColumn(name = "IDOFFICINA", referencedColumnName = "ID")
    @ManyToOne(optional = false)
    private Officina idofficina;

    public Meccanico() {
    }

    public Meccanico(Integer id) {
        this.id = id;
    }

    public Meccanico(Integer id, String nome, String cognome) {
        this.id = id;
        this.nome = nome;
        this.cognome = cognome;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getCognome() {
        return cognome;
    }

    public void setCognome(String cognome) {
        this.cognome = cognome;
    }

    @XmlTransient
    public Collection<Meccanico> getMeccanicoCollection() {
        return meccanicoCollection;
    }

    public void setMeccanicoCollection(Collection<Meccanico> meccanicoCollection) {
        this.meccanicoCollection = meccanicoCollection;
    }

    public Meccanico getIdcapoofficina() {
        return idcapoofficina;
    }

    public void setIdcapoofficina(Meccanico idcapoofficina) {
        this.idcapoofficina = idcapoofficina;
    }

    public Officina getIdofficina() {
        return idofficina;
    }

    public void setIdofficina(Officina idofficina) {
        this.idofficina = idofficina;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Meccanico)) {
            return false;
        }
        Meccanico other = (Meccanico) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.davide.model.Meccanico[ id=" + id + " ]";
    }
    
}
