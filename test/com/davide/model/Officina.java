/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.davide.model;

import java.io.Serializable;
import java.util.Collection;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Davide Ditolve 806953
 */
@Entity
@Table(name = "OFFICINA")
@NamedQueries({
    @NamedQuery(name = "Officina.findAll", query = "SELECT o FROM Officina o"),
    @NamedQuery(name = "Officina.findById", query = "SELECT o FROM Officina o WHERE o.id = :id"),
    @NamedQuery(name = "Officina.findByIndirizzo", query = "SELECT o FROM Officina o WHERE o.indirizzo = :indirizzo"),
    @NamedQuery(name = "Officina.findByPostiauto", query = "SELECT o FROM Officina o WHERE o.postiauto = :postiauto"),
    @NamedQuery(name = "Officina.findByNome", query = "SELECT o FROM Officina o WHERE o.nome = :nome"),
    @NamedQuery(name = "Officina.lastId",query ="SELECT max(o.id) FROM Officina o")})
public class Officina implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "ID")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 255)
    @Column(name = "INDIRIZZO")
    private String indirizzo;
    @Column(name = "POSTIAUTO")
    private Integer postiauto;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 255)
    @Column(name = "NOME")
    private String nome;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "idofficina")
    private Collection<Meccanico> meccanicoCollection;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "idofficina")
    private Collection<Macchina> macchinaCollection;

    public Officina() {
    }

    public Officina(Integer id) {
        this.id = id;
    }

    public Officina(Integer id, String indirizzo, String nome) {
        this.id = id;
        this.indirizzo = indirizzo;
        this.nome = nome;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getIndirizzo() {
        return indirizzo;
    }

    public void setIndirizzo(String indirizzo) {
        this.indirizzo = indirizzo;
    }

    public Integer getPostiauto() {
        return postiauto;
    }

    public void setPostiauto(Integer postiauto) {
        this.postiauto = postiauto;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    @XmlTransient
    public Collection<Meccanico> getMeccanicoCollection() {
        return meccanicoCollection;
    }

    public void setMeccanicoCollection(Collection<Meccanico> meccanicoCollection) {
        this.meccanicoCollection = meccanicoCollection;
    }

    @XmlTransient
    public Collection<Macchina> getMacchinaCollection() {
        return macchinaCollection;
    }

    public void setMacchinaCollection(Collection<Macchina> macchinaCollection) {
        this.macchinaCollection = macchinaCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Officina)) {
            return false;
        }
        Officina other = (Officina) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.davide.model.Officina[ id=" + id + " ]";
    }
    
}
