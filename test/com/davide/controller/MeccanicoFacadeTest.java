/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.davide.controller;

import com.davide.model.Meccanico;
import com.davide.model.Officina;
import java.io.File;
import java.util.HashMap;
import java.util.Map;
import javax.ejb.embeddable.EJBContainer;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Davide Ditolve 806953
 */
public class MeccanicoFacadeTest {
    
        EJBContainer container;
    
    public MeccanicoFacadeTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
        Map<String, Object> properties = new HashMap<String, Object>();
        properties.put(EJBContainer.MODULES, new File("build\\web\\WEB-INF\\classes"));
        container = javax.ejb.embeddable.EJBContainer.createEJBContainer(properties);
    }
    
    @After
    public void tearDown() {
        container.close();
    }

    /**
     * Test of insert method, of class MeccanicoFacade.
     */
    @Test
    public void testInsert() throws Exception
    {
        System.out.println("find");

        MeccanicoFacade instance = (MeccanicoFacade) container.getContext().lookup("java:global/classes/MeccanicoFacade");
        OfficinaFacade instance2 = (OfficinaFacade) container.getContext().lookup("java:global/classes/OfficinaFacade");
        
        Officina expResult1 = null;
        Officina o = new Officina();
        o.setIndirizzo("via casa sua");
        o.setNome("nome");
        o.setPostiauto(10);
        o.setId(0);
        instance2.create(o);
        
        Meccanico expResult = null;
        Meccanico m = new Meccanico();
        m.setCognome("cognome");
        m.setNome("nome");
        m.setId(0);
        //m.setIdofficina(o);
        instance.create(m);
        Meccanico result = instance.find(1);
        assertEquals("cognome", result.getCognome());
        
        instance2.remove(o);
        instance.remove(m);
    }

    /**
     * Test of edit method, of class MeccanicoFacade.
     */
    @Test
    public void testEdit() throws Exception
    {
        System.out.println("edit");

        MeccanicoFacade instance = (MeccanicoFacade) container.getContext().lookup("java:global/classes/MeccanicoFacade");
        OfficinaFacade instance2 = (OfficinaFacade) container.getContext().lookup("java:global/classes/OfficinaFacade");
        
        Officina expResult1 = null;
        Officina o = new Officina();
        o.setIndirizzo("via casa sua");
        o.setNome("nome");
        o.setPostiauto(10);
        o.setId(0);
        instance2.create(o);
        
        Meccanico expResult = null;
        Meccanico m = new Meccanico();
        m.setCognome("cognome");
        m.setNome("nome");
        m.setId(0);
        //m.setIdofficina(o);
        instance.create(m);
        
        Meccanico result = instance.find(1);
        result.setCognome("edit");
        instance.edit(result);
        result = instance.find(instance.count());
        assertEquals("edit", result.getCognome());
        
        instance2.remove(o);
        instance.remove(m);
    }

    /**
     * Test of find method, of class MeccanicoFacade.
     */
     @Test
    public void testFind() throws Exception {
        System.out.println("find");
        Object id = null;

        MeccanicoFacade instance = (MeccanicoFacade) container.getContext().lookup("java:global/classes/MeccanicoFacade");
        OfficinaFacade instance2 = (OfficinaFacade) container.getContext().lookup("java:global/classes/OfficinaFacade");
        
        Officina expResult1 = null;
        Officina o = new Officina();
        o.setIndirizzo("via casa sua");
        o.setNome("nome");
        o.setPostiauto(10);
        o.setId(0);
        instance2.create(o);
        
        Meccanico expResult = null;
        Meccanico m = new Meccanico();
        m.setCognome("cognome");
        m.setNome("nome");
        m.setId(0);
        m.setIdofficina(o);
        
        instance.create(m);
        Meccanico result = instance.find(1);
        assertEquals("cognome", result.getCognome());
        // TODO review the generated test code and remove the default call to fail.
    }
    
    /**
     * Test of remove method, of class MeccanicoFacade.
     */
    @Test
    public void testDelete() throws Exception {
        System.out.println("delete");
        MeccanicoFacade instance = (MeccanicoFacade) container.getContext().lookup("java:global/classes/MeccanicoFacade");
        OfficinaFacade instance2 = (OfficinaFacade) container.getContext().lookup("java:global/classes/OfficinaFacade");
        
        Officina expResult1 = null;
        Officina o = new Officina();
        o.setIndirizzo("via casa sua");
        o.setNome("nome");
        o.setPostiauto(10);
        o.setId(0);
        instance2.create(o);
        
        Meccanico expResult = null;
        Meccanico m = new Meccanico();
        m.setCognome("cognome");
        m.setNome("nome");
        m.setId(0);
        //m.setIdofficina(o);
        instance.create(m);
        
        instance2.remove(o);
        instance.remove(m);
    }
}
