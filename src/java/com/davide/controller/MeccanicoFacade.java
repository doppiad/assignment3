/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.davide.controller;

import com.davide.model.Meccanico;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 *
 * @author Davide Ditolve 806953
 */
@Stateless
public class MeccanicoFacade extends AbstractFacade<Meccanico> {

    @PersistenceContext(unitName = "CRUDOfficinaPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public MeccanicoFacade() {
        super(Meccanico.class);
    }
    
}
