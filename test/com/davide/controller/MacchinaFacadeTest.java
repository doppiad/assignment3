/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.davide.controller;

import com.davide.model.Cliente;
import com.davide.model.Macchina;
import com.davide.model.Officina;
import java.io.File;
import java.util.HashMap;
import java.util.Map;
import javax.ejb.embeddable.EJBContainer;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Davide Ditolve 806953
 */
public class MacchinaFacadeTest {
    
        EJBContainer container;
    
    public MacchinaFacadeTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
        Map<String, Object> properties = new HashMap<String, Object>();
        properties.put(EJBContainer.MODULES, new File("build\\web\\WEB-INF\\classes"));
        container = javax.ejb.embeddable.EJBContainer.createEJBContainer(properties);
    }
    
    @After
    public void tearDown() {
        container.close();
    }

    /**
     * Test of insert method, of class MacchinaFacade.
     */
    @Test
    public void testInsert() throws Exception
    {
        System.out.println("insert");

        ClienteFacade instance1 = (ClienteFacade) container.getContext().lookup("java:global/classes/ClienteFacade");
        MacchinaFacade instance = (MacchinaFacade) container.getContext().lookup("java:global/classes/MacchinaFacade");
        OfficinaFacade instance2 = (OfficinaFacade) container.getContext().lookup("java:global/classes/OfficinaFacade");
        
        Cliente expResult1 = null;
        Cliente c = new Cliente();
        c.setCognome("cognome");
        c.setIndirizzo("via casa sua");
        c.setNome("nome");
        c.setTelefono("222");
        c.setId(0);
        instance1.create(c);
        
        Officina expResult2 = null;
        Officina o = new Officina();
        o.setIndirizzo("via casa sua");
        o.setNome("nome");
        o.setPostiauto(10);
        o.setId(0);
        instance2.create(o);
        
        Macchina expResult = null;
        Macchina m = new Macchina();
        m.setMarca("marca");
        m.setModello("modello");
        m.setTarga("1234");
        m.setId(0);
        //m.setIdcliente(1);
       //m.setIdofficina(idofficina);
        instance.create(m);
        
        Macchina result = instance.find(1);
        assertEquals("marca", result.getMarca());
        instance1.remove(c);
        instance2.remove(o);
        instance.remove(m);
    }

    /**
     * Test of edit method, of class MacchinaFacade.
     */
    @Test
    public void testEdit() throws Exception
    {
        System.out.println("find");

        ClienteFacade instance1 = (ClienteFacade) container.getContext().lookup("java:global/classes/ClienteFacade");
        MacchinaFacade instance = (MacchinaFacade) container.getContext().lookup("java:global/classes/MacchinaFacade");
        OfficinaFacade instance2 = (OfficinaFacade) container.getContext().lookup("java:global/classes/OfficinaFacade");
        
        Cliente expResult1 = null;
        Cliente c = new Cliente();
        c.setCognome("cognome");
        c.setIndirizzo("via casa sua");
        c.setNome("nome");
        c.setTelefono("222");
        c.setId(0);
        instance1.create(c);
        
        Officina expResult2 = null;
        Officina o = new Officina();
        o.setIndirizzo("via casa sua");
        o.setNome("nome");
        o.setPostiauto(10);
        o.setId(0);
        instance2.create(o);
        
        Macchina expResult = null;
        Macchina m = new Macchina();
        m.setMarca("marca");
        m.setModello("modello");
        m.setTarga("1234");
        m.setId(0);
        //m.setIdcliente(1);
       //m.setIdofficina(idofficina);
        instance.create(m);
        
        Macchina result = instance.find(1);
        result.setMarca("edit");
        instance.edit(result);
        result = instance.find(instance.count());
        assertEquals("edit", result.getMarca());
       
        instance1.remove(c);
        instance2.remove(o);
        instance.remove(m);
    }


    /**
     * Test of find method, of class MacchinaFacade.
     */
     @Test
    public void testFind() throws Exception {
        System.out.println("find");
        Object id = null;

        ClienteFacade instance = (ClienteFacade) container.getContext().lookup("java:global/classes/ClienteFacade");
              Cliente c = new Cliente();
        c.setCognome("cognome");
        c.setIndirizzo("via casa sua");
        c.setNome("nome");
        c.setTelefono("222");
        c.setId(0);
        instance.create(c);
        Cliente expResult = null;
        Cliente result = instance.find(1);
        assertEquals("cognome", result.getCognome());
        // TODO review the generated test code and remove the default call to fail.
    }
    
    /**
     * Test of remove method, of class ClienteFacade.
     */
    @Test
    public void testDelete() throws Exception {
        System.out.println("delete");
        ClienteFacade instance = (ClienteFacade) container.getContext().lookup("java:global/classes/ClienteFacade");
        Cliente expResult = null;
        Cliente c = new Cliente();
        c.setCognome("cognome");
        c.setIndirizzo("via casa sua");
        c.setNome("nome");
        c.setTelefono("222");
        c.setId(0);
        instance.create(c);
        instance.remove(c);
    }
}