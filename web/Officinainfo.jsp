<%-- 
    Document   : index
    Created on : 6-dic-2018, 22.43.05
    Author     : Davide
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <!-- Latest compiled and minified CSS -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
<script src="https://code.jquery.com/jquery-3.3.1.min.js"/>
<!-- Optional theme -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css" integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp" crossorigin="anonymous">

<!-- Latest compiled and minified JavaScript -->
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
 <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.5.0/css/all.css" integrity="sha384-B4dIYHKNBt8Bc12p+WXckhzcICo0wtJAoU8YZTY5qE0Id1GSseTk6S+L3BlXeVIU" crossorigin="anonymous">

<title>Informazione clienti</title>
    </head>
    <body>
        <%@include file="header.jsp" %>
        <h1>Cliente Information</h1>
        <form action="./OfficinaServlet" method="POST">
            <table class="table">
                <tr hidden="hidden">
                    <td>ID Cliente</td>
                    <td><input class="form-control" type="hidden" name="id" value="${officina.id}" /></td>
                </tr>
                <tr>
                    <td>Nome</td>
                    <td><input class="form-control" type="text" name="nome" value="${officina.nome}" /></td>
                </tr>
                <tr>
                    <td>Posti auto</td>
                    <td><input class="form-control" type="text" name="postiauto" value="${officina.postiAuto}" /></td>
                </tr>
                <tr>
                    <td>Indirizzo</td>
                    <td><input class="form-control" type="text" name="indirizzo" value="${officina.indirizzo}" /></td>
                </tr>
                <tr>
                    <td colspan="2">
                        <input class="btn btn-default" type="submit" name="action" value="Add" />
                        <input class="btn btn-default" type="submit" name="action" value="Edit" />
                        <input class="btn btn-default" type="submit" name="action" value="Delete" />
                        <input class="btn btn-default" type="submit" id="load" name="action" value="Search" />
                    </td>                
                </tr>            
            </table>
        </form>        
        <br>
        <table border="1" class="table">
            <th>ID</th>
            <th>Nome</th>
            <th>Cognome</th>
            <th>Indirizzo</th>
            <th>Telefono</th>
            <c:forEach items="${allOfficine}" var="of">
                <tr>
                    <td>${of.id}</td>
                    <td>${of.nome}</td>
                    <td>${of.postiauto}</td>
                    <td>${of.indirizzo}</td>
                </tr>
            </c:forEach>
        </table>  
        <a href="index.jsp"><i class="fas fa-undo-alt"></i> Back to menu</a>
    </div>

    </body>
</html>
