/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.davide.model;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 *
 * @author Davide Ditolve 806953
 */
@Entity
@Table(name = "MACCHINA")
@NamedQueries({
    @NamedQuery(name = "Macchina.findAll", query = "SELECT m FROM Macchina m"),
    @NamedQuery(name = "Macchina.findById", query = "SELECT m FROM Macchina m WHERE m.id = :id"),
    @NamedQuery(name = "Macchina.findByMarca", query = "SELECT m FROM Macchina m WHERE m.marca = :marca"),
    @NamedQuery(name = "Macchina.findByModello", query = "SELECT m FROM Macchina m WHERE m.modello = :modello"),
    @NamedQuery(name = "Macchina.findByTarga", query = "SELECT m FROM Macchina m WHERE m.targa = :targa"),
    @NamedQuery(name = "Macchina.lastId",query ="SELECT max(m.id) FROM Macchina m")})
public class Macchina implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "ID")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 255)
    @Column(name = "MARCA")
    private String marca;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 255)
    @Column(name = "MODELLO")
    private String modello;
    @Size(max = 255)
    @Column(name = "TARGA")
    private String targa;
    @JoinColumn(name = "IDCLIENTE", referencedColumnName = "ID")
    @ManyToOne(optional = false)
    private Cliente idcliente;
    @JoinColumn(name = "IDOFFICINA", referencedColumnName = "ID")
    @ManyToOne(optional = false)
    private Officina idofficina;

    public Macchina() {
    }

    public Macchina(Integer id) {
        this.id = id;
    }

    public Macchina(Integer id, String marca, String modello) {
        this.id = id;
        this.marca = marca;
        this.modello = modello;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getMarca() {
        return marca;
    }

    public void setMarca(String marca) {
        this.marca = marca;
    }

    public String getModello() {
        return modello;
    }

    public void setModello(String modello) {
        this.modello = modello;
    }

    public String getTarga() {
        return targa;
    }

    public void setTarga(String targa) {
        this.targa = targa;
    }

    public Cliente getIdcliente() {
        return idcliente;
    }

    public void setIdcliente(Cliente idcliente) {
        this.idcliente = idcliente;
    }

    public Officina getIdofficina() {
        return idofficina;
    }

    public void setIdofficina(Officina idofficina) {
        this.idofficina = idofficina;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Macchina)) {
            return false;
        }
        Macchina other = (Macchina) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.davide.model.Macchina[ id=" + id + " ]";
    }
    
}
