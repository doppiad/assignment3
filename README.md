Assignment 3
Processo e sviluppo software – Consegna 2 Ditolve Davide 806953 Gennaio 2019


1	Descrizione applicativo

L’applicativo consiste in una webapplication per gestire le auto presenti in un catena di officine meccaniche e per gestire le anagrafiche clienti e dipendenti. In particolar modo è possibile tener traccia di quali macchine sono presenti in una data officina, quali dipendenti lavorano nella stessa e chi tra i vari   dipendenti risulta essere il capo officina.
L’applicazione nella sua prima versione è molto semplice e permette di effettuare tutte le operazioni   basilari (CRUD) compresa l’operazione di search che nella prima versione sarà disponibile solo attraverso l’id. L’utilizzo dell’applicativo è possibile attraverso un interfaccia web realizzata in JSP. Questa interfaccia è stata graficamente abbellita mediante l’utilizzo di alcuni CSS e JS disponibili online, in particolare Bootstrap e Fontawesome.
Nella seconda versione dell’applicazione, l’interfaccia è stata migrata in apache JSF in quanto è un framework più nuovo rispetto al classico JSP ed inoltre è più versatile rispetto a quest ultimo. Viene comunque lasciato nella directory del progetto ogni riferimento all’implementazione JSP questo per mettere in risalto le differenze tra I due tipi di implementazione. Inoltre è possibile notare come l’implementazione JSP è stata fatta monopagina mentre in JSF ho scelto di dividere tutte le funzione in pagine dedicate, salta inoltre subito all’occhio come principale differenza l’aspetto grafico, che su JSP avevo curato maggiormente.

Scelte Implementative
Netbeans
Ho optato per netbeans come ide perché tra i vari software disponibili in commercio, mi è sembrata la scelta migliore in quanto è in grado di gestire i javaDB e i server GlassFish direttamente dall’interfaccia. Inoltre ho preferito netbeans a Eclipse perché in passato ho già utilizzato questo software.


Glassfish server
Ho preferito Glassfish a TOMCAT in quanto mi serviva un server in grado di gestire non solo il deploy di un pacchetto war ma anche la possibilità di accomunare jdbc resources sotto un unico pool di connessione. In tomcat questa cosa non è nativa e va costruita artificialmente attraverso l’utilizzo di moduli terzi, in glassfish invece questo mi permette di utilizzare un DB locale senza particolari modifiche al codice. Inoltre glassfish è completamente compatibile con EJB e JPA in particolare infatti ho optato per JPA2.1


EJB
Ho deciso di utilizzare Enterprise java beans per fornire al front end elementi della business logic (backend)
Java 8
In particolare ho scelto la versione 8 perché è quella che offre più stabilità ed è la versione con più documentazione disponibile online. Java 8 inoltre rimane retrocompatibile con chi ha la versione nuova ovvero la 11.
Bootstrap e Fontawesome
Ho scelto di utilizzare questi CSS disponibili gratuitamente online per personalizzare l’interfaccia utente. In particolare queste due scelte sono date dal fatto che sono i componenti web più diffusi e semplici da utilizzare.
Bootstrap è diventato nel tempo il framework frontend più utilizzato al mondo. Sviluppato originariamente da Twitter, è un progetto open source con un altissimo numero di contributi. Un insieme di codice Javascript e fogli di stile css con diverse classi pronti ad essere utilizzati per un insieme di componenti pronti e personalizzabili che possono essere adattati ad ogni schermo. Bootstrap è uno dei pochi framework destinato sia ai dispositivi desktop che al mobile, presentando in maniera responsive qualsiasi oggetto utilizzato. Ogni classe di default di Boostrap ha associate le media query relative ad un grande insieme di dispositivi (o forse sarebbe meglio dire di dimensioni schermo differenti). È possibile inoltre personalizzare l'aspetto di ogni componente cambiando le dimensioni in base alle diverse grid-class xs (phones), sm (tablets), md (desktops), and lg (larger desktops).

JQuery

JQuery è una libreria JavaScript che crea all'interno del linguaggio javascript un ambiente funzionale interpretato esclusivamente dal relativo framework. E' nato con il fine di semplificare la selezione, la manipolazione, la gestione degli eventi e l'animazione di elementi DOM in pagine HTML, oltre ad implementare funzionalità richieste web con metodologia AJAX. Nel tempo si sono sviluppati diversi plugin ed utility che necessitano di jquery per via della semplicità con cui è possibile gestire le modifiche alle classi in maniera dinamica. Ad esempio $('id').html=ciao modifica l'elemento del dom "id" assegnandogli il relativo contenuto, in JavaScript nativo per fare qualcosa del genere è necessario risalire al padre dell'elemento da modificare, eliminare il child, salvare tutti i child successivi ed eliminarli, inserire il nuovo elemento come child dell'elemento padre ed infine reinserire il resto degli elementi dom. Un'operazione apparentemente banale equivale a due funzioni ed una dozzina di righe di codice. Inoltre Bootstrap, precedentemente citato, richiede jQuery che viene utilizzato per tutti i componenti dinamici come ad esempio tabelle, navbar, searchbar, form ecc.
ApacheDB
Ho scelto come motore di DB ApacheDB in quanto è completamente integrato in glassfish ed è gestibile da NetBeans.


2	MODELLO ER


Ho usato STARUML per generare il precedente diagramma. Meccanico
Questa entità consente di avere l’anagrafica dei meccanici e tramite la relazione di supervisionato è  possibile sapere se un dato meccanico è anche capo officina, altrimenti si puo’ sapere chi è il suo superiore. Tramite la relazione lavorano invece è possibile sapere in quale officina presta servizio un dato meccanico. Nella prima versione l’anagrafica è molto semplice e consente di inserire e leggere solamente nome e cognome.


Officina
Consente di tenere traccia di tutte le officine facenti parte della rete, in particolare possiamo inserire per queste un indirizzo un nome e la quantità di posti auto disponibili (anche se quest’ultima nella prima versione non viene gestita in alcun modo). Tramite le proprietà di navigazione inverse è possibile sapere quali meccanici ci lavorano e quali auto sono presenti.
 
Macchine
Questa entità consente di dare un volto alla macchina, una macchina è caratterizzata da una marca un modello e un cliente, inoltre viene anche detto in quale officina si trova. Nella prima versione non è presente la targa ma viene utilizzato un id progressivo per la gestione dell’univocità.


Cliente
Questa entità consente di inserire una anagrafica di base per la gestione della clientela. Vengono richiesti i campi indispensabili per richiamare il cliente al termine della riparazione.

Le classi dei model sono state mantenute anche nella seconda versione tali e quali per non intervenire su JSP e JSF perchè come spiegato nella sezione 1 ho volute mettere in risalto la differenza nell’implementazione di applicazioni web con 2 tecnologie diverse quali JSF e JSP. Rimane comunque possibile estendere I modelli in maniera molto facile, in particolare Java a differenza di Asp.net consente di gestire il DB sia in versione model first sia in versione Codefirst senza modifiche al codice, questo significa che possiamo estendere i modelli aggiungendo entità sul db e utilizzando netbeans aggiornare I model oppure viceversa settare il file persistence.xml su create and drop e aggiungere campi alle nostre classi software, all’avvio del software eclipselink si preoccuperà di ricreare il DB con la nuova struttura.

 
Modellazione code first 
Questo approccio di scrittura del codice prevede la modellazione del DB relazionale prima sotto forma di classi proprie del codice, ovvero andando a definire quelli che sono i models all’interno del pattern MVC, si modellano le relazioni mediante l’uso di tipi complessi o collection degli stessi. Sarà poi il framework a trasportare attraverso un’operazione chiamata “migrazione” il codice in tabelle e campi con il giusto formato compatibile con quello scelto per i dati.

Test
Nella prima versione sono stati costruiti gli scheletri di tutte le funzioni per testare completamente il software, tuttavia saranno implementate nel codice e nel corpo nella seconda versione della consegna.

Nella seconda versione sono stati introdotti I test per l’applicazione, in particolare mi sono focalizzato sull’effettuare test di funzionalità dell’applicativo dando per scontato che eclipselink se correttamente configurato esegua correttamente le operazioni CRUD, per tanto non sono stati implementati integration test sui DAO. I test sono stati effettuati utilizzando 2 framework, Junit 4 e Selenium, il primo è presente in tutti I test, è il motore principale di testing per java ed è stato usato per I test di funzionalità e sui model, mentre selenium è stato utilizzato per testare che effettivamente il frontend si comporti in maniera coerente con quanto atteso. Selenium in particolare ci permette di registrare delle macro sull’applicazione e tramite id ripete il test n volte, inoltre la potenzialità sta nel fatto che è possibile eseguire un test data driven dando in pasto a selenium un CSV di valori che si vuole che vengano testate in maniera automatica
 

Sviluppi futuri:
Aggiungere più campi alle tabelle già presenti. Migliorare l’aspetto grafico delle pagine JSP. Spostare il DB da locale a remoto così da poter deployare il progetto in remoto senza necessità di compilarlo e avviarlo localmente. Completare i test. Aggiungere una nuova entità per gestire le riparazioni.

Conclusioni:

Non sono riuscito a spostare correttamente il DB su un istanza azure di SqlServer in quanto fortemente dipendente da GlassFish che non è compatibile con il servizio APP di azure. Non ho aggiunto entità come già specificato, perchè ho volute mettere in risalto la differenza tra JSP e JSF. 
Essendo un DB molto semplice e integrato sono stati volutamente evitati I test di integrazione, in quanto viene dato per assodato che il DB sia perfettamente integrato con l’app. Essendo il DB considerato integrato, sono stati eseguiti I test di funzionalità con selenium per testare se gli inserimenti, le modifiche e le cancellazioni vanno a buon fine partendo da valori considerati corretti al momento della scrittura del test. Questo significa che si da per scontato che se il modello dati è compilato in maniera corretta le operazioni crud verranno delegate correttamente al DB, viceversa se il modello è sbagliato, la validazione ci avvertirà di possibili errori.


 


