/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.davide.controller;

/**
 *
 * @author Davide
 */
public class StaticVariables {

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 67 * hash + (this.loaded ? 1 : 0);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final StaticVariables other = (StaticVariables) obj;
        if (this.loaded != other.loaded) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "StaticVariables{" + "loaded=" + loaded + '}';
    }

    public boolean isLoaded() {
        return loaded;
    }

    public void setLoaded(boolean loaded) {
        this.loaded = loaded;
    }
    
    private boolean loaded;
    
    private StaticVariables() {
        loaded = false;
    }
    
    public static StaticVariables getInstance() {
        return StaticVariablesHolder.INSTANCE;
    }
    
    private static class StaticVariablesHolder {

        private static final StaticVariables INSTANCE = new StaticVariables();
    }
}
