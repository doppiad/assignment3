/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.davide.dao;

import com.davide.model.Officina;
import java.util.List;
import javax.ejb.Local;

/**
 *
 * @author Davide
 */
@Local
public interface OfficinaDAOLocal {
    public void addOfficina(Officina officina);
    public void editOfficina(Officina officina);
    public void deleteOfficina(int idOfficina);
    public Officina getOfficina(int idOfficina);
    public List<Officina> getAllOfficine();
     public int getLastId();
}
